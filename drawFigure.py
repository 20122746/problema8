from projectionFigure import *
from projection import *
import tkinter as TK    # Inclui a biblioteca com as funções do GUI de TKinter

# Variaveis globais para a definição da janela respetivamento do Canvas
h=600            # A altura da janela 
w=450            # O comprimento da janela


# Main programm
if __name__ == '__main__':
    table = readLineForLineFromFile("figure.txt")     # Extrair o ficheiro "figure.txt
    P = ~(extractPoints(table, 3)*projection(-0.2))   # Extrai da tabela "table" a matriz dos pontos
                                                      # Applicando a projeção e normalizar la
    Polygons = extractPolygons(table)                 # Extrai da tabela "table" os indícies dos polígonos

    # calcular a matriz de pontos de acordo com os requisitos do problema
    m2 = ~(extractPoints(table, 3) * translation([20, 10, -2]) * projection(-0.2))

    # Make the GUI of the program
    windows = TK.Tk()
    image = TK.Canvas(windows, cursor="arrow", bg="yellow", height=h, width=w)
    image.pack()

    for i in range(len(Polygons)):
        for j in range(len(Polygons[i])):
            image.create_line(P.getValue(Polygons[i][j], 0), P.getValue(Polygons[i][j], 1),
                              P.getValue(Polygons[i][(j+1) % len(Polygons[i])], 0),
                              P.getValue(Polygons[i][(j+1) % len(Polygons[i])], 1),
                              fill='red')

            # desenhar o poligono solicitado pelo problema
            image.create_line(m2.getValue(Polygons[i][j], 0), m2.getValue(Polygons[i][j], 1),
                              m2.getValue(Polygons[i][(j + 1) % len(Polygons[i])], 0),
                              m2.getValue(Polygons[i][(j + 1) % len(Polygons[i])], 1),
                              fill='blue')


    # LOOP
    windows.mainloop()
