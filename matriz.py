# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# C L A S S E :     M A T R I Z          --> como lista de vetores
#
#                   M E M B R O S:        value       --> A lista que contem todos os vetores da matriz
#
#                   S E R V I Ç O S:    CRIA MATRIZ:            a) __init__             --> Operador da initialização de uma nova matriz
#                                       METÒDOS:                1) showMatrix           --> Representar a matriz no shell
#                                                               2) dim                  --> Dar a dimensão da matriz como um tuple (linha,coluna)
#                                                               3) getValue             --> Retorna um valor da matrix numa posição especifica
#
#                                       OPERADORES:             __mul__                 --> * (Multiplicação matricial
#                                                               __invert__              --> ~ (Transposição da matriz)             
#                                                               

class matriz:
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # C R I A Ç Ã O      de uma      M A T R I Z            a) __init__             --> Inicialização de uma matriz nova

    # ==================================================================================
    # Função               --> __init__: Initialisa uma matriz a partir de uma lista dada, analisando
    #                             se esta é uma lista de list com as seguintes propriedades:
    #                              1) uma lista não vázia    
    #                              2) todos elementos da lista inicial são não vazias
    #                              3) tendo tudos as mesmas números de elementos
    #                           Caso contrário passamos uma matriz vazia. 
    # Entradas             --> valueList: uma lista de listas, onde as listas inteira são as linhas da matriz.
    # Saída                --> Quando 1)-3) é válido, obtemos uma matrix com os valores dado da valueList
    #
    def __init__(self, valueList):
        # verifique se a valueList é não vazio com um promeiro elemento não vazio
        if (type(valueList) != list or len(valueList) < 1 or type(valueList[0]) != list or len(valueList[0]) < 1):
            # Quando não é voltamos uma matriz vazia 
            self.value = [[]]
            return
            # verifique se os restantes elementos tenhãm o mesmo tamanho que o primeiro
        for i in range(len(valueList)):
            if (type(valueList[i]) != list or len(valueList[i]) != len(valueList[0])):
                # Quando não é voltamos uma matriz vazia
                self.value = [[]]
                return

        # Sendo ser uma matriz válida podemos copiar os valores de entrada para a matriz atual
        self.value = []
        for i in range(len(valueList)):
            line = []
            for j in range(len(valueList[0])):
                line.append(valueList[i][j])
            self.value.append(line)
        return
        # ==============================================================================

    # ==============================================================================

    # FIM da                      --->      C R I A T Ç Ã O   de uma      M A T R I Z
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # METÓDOS:                          1) showMatrix           --> Representação da matriz no ecran
    #                                   2) dim                  --> Retornar a dimensão da matriz como um tuple (linha,coluna)
    #                                   3) getValue             --> Retorna um valor da matrix numa posição especifica
    # ===============================================================================
    # Função  --> showMatrix: Apresenta os valores da matriz linha à linha no ecran
    #
    # Entrada           --> virtual: Matriz como uma lista de listas (listas inteiras são as linhas da matriz)
    # Efeito secundário --> Apresentaçaõ no ecran dos valores da matriz linha à linha

    def showMatrix(self):
        # Apresenta no ecran todos os valores da matriz linha à linha
        for i in range(len(self.value)):
            print(self.value[i])

    # ==============================================================================

    # ==============================================================================
    # Função      --> dim: Dimensão da matriz
    #
    # Entrada     --> virtual: Matriz como uma lista de listas (listas inteiras são as linhas da matriz)
    # Saída       --> A dimensão da instância da matriz em forma de (linhas, colunas)
    #
    def dim(self):
        if (len(self.v) <= 0 or len(self.v[0]) <= 0): return (0, 0)
        return (len(self.v), len(self.v[0]))

    # ==============================================================================
    # Função      --> getValue: Retorna o valor da matriz numa posição especifica
    #
    # Entrada     --> (line,column): Indica uma posição da Matriz para retornar
    # Saída       --> Retorna o valor na posição (line,column) da Matriz
    #

    def getValue(self, line, column):
        return (self.value[line][column])

    # ==============================================================================

    # FIM do                -->                METÓDOS
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # O P E R A T O R S:    - ADIÇÃO (+)
    # -------------------------------------------------------------------------------

    # ==============================================================================
    # Função   --> __mul__: OPERADOR (*) da Multiplicação da matriz "self" com uma outra matriz "B"
    #
    # Entrada    -->  virtual: a própria matriz / matriz B para adicionar
    #
    # Saída      --> Matriz R=self*B
    #                Em caso de errors o resultado seria uma lista vázia "[[]]"

    def __mul__(self, B):
        # Verifique se as matrizes formalmente podem se multiplicar
        # Comecanndo com a definição dos dimensõeses de ambas as matrizes
        linA = len(self.value)  # O númbero das linhas da matriz "self"
        colA = len(self.value[0])  # O númbero das colunas da matriz"self"
        linB = len(B.value)  # O númbero das linhas na matriz "B"
        colB = len(B.value[0])  # O númbero das colunas da matriz "B"
        if (colA != linB):
            return (matriz([[]]))  # Caso que náo podem multiplicar, retourna uma matriz vázia
        else:  # Caso contrário, prepara a multiplicação
            R = []  # Defina a list exterior da matriz resultando

        # Calula o produto entre "self" e "B"
        for i in range(linA):  # define a linha "i" para preebcher todos os R[i][j]
            val = []  # prepara a iº linha in R
            for j in range(colB):  # defina a coluna "j" para um especifico valor R[i][j]
                accumulate = 0  # inicializar o accumulador para R[i][j] ao zero
                for k in range(linB):  # accumula todos os produto dos pares
                    accumulate += self.value[i][k] * B.value[k][j]  # accumula o actual produto dos pares
                val.append(accumulate)  # insere o resultado da accumulação na line "i"
            R.append(val)  # insere toda a line "i" para a R
        return (matriz(R))  # retourna o result R

    # ==============================================================================
    # Função --> __invert__ - OPERADOR (~): Transposição da matriz "self"
    #
    # Entrada    -->  virtual: a própria matriz "self"
    #
    # Saída      --> Matriz da transposição
    #                Em caso de errors o resultado seria uma lista vázia "[[]]"

    def __invert__(self):
        # valores da ultima coluna
        last_col = []

        #   Se não for uma matriz nula, assume-se que é m x n
        if self.value != [[]]:
            zero = False
            for i in self.value:
                # copia os valores da ultima coluna para uma matriz à parte
                last_col.append(i[-1])
                if i[-1] == 0:
                    zero = True

            # se não existirem valores = 0 na ultima coluna
            if not zero:
                r = matriz(self.value)
                for lin in range(0, len(r.value)):
                    for col in range(0, len(r.value[lin])):
                        r.value[lin][col] /= last_col[lin]
            else:
                r = matriz([])

        else:
            r = matriz([])
        return r
    # ==============================================================================

    # ==============================================================================
    # FIM de                            -->  O P E R A D O R E S
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


# ****************************************************************************************
# M A I N - F U N C T I O N
# ------------------------------------------------------------------    
# 1) Calculate the transposition Matrix for a camera at point (500,500,500) with the vector
#    (-150,-100,-150) on its visualization plane into to the standart projection plane of XY.
# 2) Applying this transposition also to the Cube in the origem with the length 100
# ------------------------------------------------------------------
def __main__():
    a = matriz([[3, 4, 0], [4, 8, 4]])
    b = matriz([[3, 4, 0, 6], [4, 8, 4, 2], [5, 1, 2, 6]])

    a.showMatrix()
    print
    inv_a = ~a
    inv_a.showMatrix()

    print
    again = ~inv_a
    again.showMatrix()

    print()
    b.showMatrix()
    inv_b = ~b
    print
    ~b.showMatrix()






# Executa a função __main__() quando o programa é executivel,
# mas não quando e importado
if __name__ == "__main__":
    __main__()
