from matriz import matriz
import math
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# M O D U L :     T R A N S F O R M A T I O N          --> Cria matrizes especificas
#
#                 F U N Ç Õ E S:     CRIA TRANSFORAÇÕES:         a) identityList
#                                                                b) tanslation
#                                                                c) rotation
#                                                                d) dilatation
#                                                                e) projection
#                                                                
# ==============================================================================
# Função   --> identity: Cria a lista para a matriz de identidade, isto é uma matriz 
#                        só com zeros exeptando com ums na diagonal cima esquerda para
#                        baixo direita
#
# Entrada  --> "dim": a dimensão desta matriz quadrada 
# Saída    --> Uma lista que serve para criar a corespondente matriz de identidade
def idendityList(dim):
    value=[]
    # Cria a matriz da identidade em dimxdim
    for i in range(0,dim):
        value.append([])
        for j in range(0,dim):
            if i==j: value[i].append(1)
            else:value[i].append(0)
    return(value)


# ==============================================================================
# Função   --> translation: Cria de uma lista com dois elementos a correspondente matriz
#                           de translação no hyper-espaço homogéneo
#
# Entrada  --> "v": uma lista com dois ou três números considerado com um vetor plano ou 3D 
# Saída    --> A corespondente matriz de translão para v no no hyper-espaço homogéneo
#              Se v não tem o formato esperado, então produz a matriz de identidade
def translation(v):
    # Se a translação não fosse no plano ou em 3D, devolve uma matriz vazia
    if len(v)<2 or len(v)>3: return(matriz([[]]))
    # Cria a matriz da identidade em (dim(v)+1)x(dim(v)+1)
    L=idendityList(len(v)+1)
    # Substitui a última linha pelo v, mantendo a última posição com o valor 1
    for j in range (0,len(v)): L[len(v)][j]=v[j]
    return(matriz(L))
# ==============================================================================

# ==============================================================================
# Função     --> rotation: Cria a partir do ânuglo a corespondente matriz da rotação
#
# Entrada    --> "alpha": O ângulo de rotação em radians
#                "dim": Espaço da rotação --> 2=plano;3=3D
#                "axis": O eixo da rotação --> 0=exio X;1=eixo Y; 2=eixo Z
# Saída      --> A corespondente matriz da rotação para o ângulo dado
#                Caso que o ângulo não bate certe, retorna a matriz de identidade em 3x3
def rotation(alpha,dim=2,axis=2):
    # Verifique que o argumento é um valor númerico; a dimensão e entre 2 e 3; bem como
    # o eixo da rotação é compartible com a dimensão
    if type(alpha) != float or dim<2 or dim>3 or axis>2 or axis<0 or (axis==2 and axis!=2):
        # Dado do formato erado retorna a matriz vazia
        return(matriz([[]]))
    # A rotação no plano é sobre o eixo Z, então unicamente definido
    if dim==2:return(matriz([[math.cos(alpha),math.sin(alpha),0],[-math.sin(alpha),math.cos(alpha),0],[0,0,1]]))
    if axis==0: return(matriz([[1,0,0,0],[0,math.cos(alpha),math.sin(alpha),0],[0,-math.sin(alpha),math.cos(alpha),0],[0,0,0,1]]))
    if axis==1: return(matriz([[math.cos(alpha),0,math.sin(alpha),0],[0,1,0,0],[-math.sin(alpha),0,math.cos(alpha),0],[0,0,0,1]]))
    return(matriz([[math.cos(alpha),math.sin(alpha),0,0],[-math.sin(alpha),math.cos(alpha),0,0],[0,0,1,0],[0,0,0,1]]))                   
# ==============================================================================

# ==============================================================================
# Função   --> dilatation: Cria para a lista de dois ou três elementos
#                          a correspondente matriz da dilatação no
#                          hyper-espaço homogéneo
#
# Entrada  --> v: A lista com dois ou três números considerado com um vetor plano ou 3D 
# Saída    --> A corespondente matriz de translão para v no no hyper-espaço homogéneo
#              Se v não tem o formato esperado, então produz a matriz de identidade
def dilatation(v):
    # Verifique se o vetor seja valido
    if len(v)<2 or len(v)>3: return(matriz([[]])) 
    for i in range(len(v)):
      if (type(v[i]) != int and type(v[i]) != float):return(matriz([[]]))
        
    # Cria a matriz da identidade em (len(v)+1)x(len(v)+1)
    L=idendityList(len(v)+1)
    # Adapta a diagonal
    for i in range(len(v)):
        L[i][i]=v[i]
    return(matriz(L))
# ==============================================================================

# ==============================================================================
# Função   --> projection: Sendo a camara em cima do negative parte do eixo z
#                          virado diretamente para origem a camera tem o plano
#                          de projecção o plano XY
#
# Entrada  --> "d" posição da câmera no eixo z
# Saída    --> A matriz da projeção

def projection(d):
    # verifique que a câmera esta atrás do plano XY, senão devolva a matriz vazia
    if (d>=0): return(matriz([[]]))
    # Começa com a Lista para a matriz de identidade 4x4
    L=idendityList(4)
    L[2][2]=0
    L[2][3]=d
    return(matriz(L))
# ==============================================================================

# ==============================================================================
# Função   --> transformação: aplica uma transformação a uma matriz
## Entrada  --> "d"
# Saída    --> A matriz transformada

def transformar(entrada: matriz, d = -0.2) -> matriz:
    # criar a matriz de transformação
    transf = matriz([[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 0, d],
                     [0, 0, 0, 1]])

    # multiplicar as matrizes
    temp = entrada * transf
    # inverter com a função desenvolvida por nós
    resultado = ~temp

    # devolver o resultado
    return resultado


# ===============================================================================

# FIM dos                -->                FUNÇÕES
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

# ****************************************************************************************
# M A I N - F U N C T I O N
# ------------------------------------------------------------------    
# Calculate para os pontos (0,0), (3,3), (3,0) e (0,3) a dilatação com a=100 e b=150.
def __main__():
    # criar a matriz com os pontos
    pontos = matriz([[3, 3, 0, 1],
                     [3, 3, 3, 1],
                     [0, 3, 3, 1],
                     [3, 0, 3, 1],
                     [0, 0, 3, 1],
                     [0, 0, 0, 1]])

    res = transformar(pontos)
    res.showMatrix()


# Executa a função __main__() quando o programa é executivel,
# mas não quando e importado
if __name__=="__main__":
    __main__()
